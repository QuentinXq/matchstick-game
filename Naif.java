package allumettes;
import java.util.Random;

public class Naif implements Strategie {
    /** Permet de choisir un nombre aléatoire d'allumettes
     * @param jeu jeu sur lequelle s'applique la prise
     * @return nombre d'allumettes
     */
    @Override
    public int getPrise(Jeu jeu) {
        Random naifRandom = new Random();
        if (jeu.getNombreAllumettes() > Jeu.PRISE_MAX) {
            return naifRandom.nextInt(Jeu.PRISE_MAX) + 1;
        } else {
            return naifRandom.nextInt(jeu.getNombreAllumettes()) + 1;
        }
    }
}
