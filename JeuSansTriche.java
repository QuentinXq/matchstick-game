package allumettes;

public class JeuSansTriche implements Jeu {
    private Jeu jeu;

    /** Construit l'objet JeuSansTriche.
     * @param jeu jeu sur lequel s'appuie le constructeur
     */
    public JeuSansTriche(Jeu jeu) {
        this.jeu = jeu;
    }

    /** Permet d'obtenir le nombre d'allumettes.
     * @return nombre d'allumettes
     */
    @Override
    public int getNombreAllumettes() {
        return this.jeu.getNombreAllumettes();
    }

    /** Permet de retirer des allumettes dans le jeu
     * et de lancer une exception.
     * @param nombreAllumettesPrises
     * @throws CoupInvalideException
     */
    public void retirer(int nombreAllumettesPrises) throws CoupInvalideException {
        throw new OperationInterditeException("Triche détectée");
    }
}
