package allumettes;

public class Tricheur implements Strategie {
    /** Permet de tricher afin de laisser qu'une seule allumette
     * afin de gagner.
     * @param jeu jeu sur lequelle s'applique la prise
     * @return nombre d'allumettes
     */
    @Override
    public int getPrise(Jeu jeu) {
        while (jeu.getNombreAllumettes() > 2) {
            try {
                jeu.retirer(1);
            } catch (CoupInvalideException e) {
                System.out.println("Tu n'as pas le droit de faire ça");
                return 0;
            }
        }
        return 1;
    }
}
