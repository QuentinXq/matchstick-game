package allumettes;
import java.util.Scanner;

public class Humain implements Strategie {

    private Scanner choixJoueur;

    /** Construit l'objet Humain en initialisant le Scanner.
     */
    public Humain() {
        choixJoueur = new Scanner(System.in);
    }

    /** Permet de demander au joueur le nombre d'allumettes.
     * @param jeu jeu sur lequelle s'applique la prise
     * @return nombre d'allumettes
     */
    @Override
    public int getPrise(Jeu jeu) {
        do {
            try {
                System.out.print("Combien prenez-vous d'allumettes ? ");
                return Integer.parseInt(choixJoueur.next());
            } catch (NumberFormatException e) {
                System.out.println("Vous devez donner un entier.");
            }
        } while (true);
    }
}
