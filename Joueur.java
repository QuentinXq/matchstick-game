package allumettes;

/** Modélise un joueur en fonction de son nom.
 * @author Quentin Rostalski
 * @version 1.0
 */
public class Joueur {
    /** Nom du joueur */
    private String nom;

    /** Stratégie du joueur */
    private Strategie strategie;

    /** Créer le joueur avec son nom et sa stratégie.
     * @param nomStrategie argument rentré en lançant le programme sous la forme
     * nom@stratégie
     */
    public Joueur(String nom, Strategie strategie) {
        this.nom = nom;
        this.strategie = strategie;
    }

    /** Retourne le nom du joueur.
     * @return nom du joueur
     */
    public String getNom() {
        return this.nom;
    }

    /** Permet de demander à un joueur le nombre d'allumettes qu'il veut prendre
     * pour un jeu donné.
     * @param jeu jeu sur lequel le joueur va agir
     * @return le nombre d'allumettes choisit par le joueur
     */
    public int getPrise(Jeu jeu) {
        return this.strategie.getPrise(jeu);
    }

    /** Permet de modifier la stratégie du joueur.
     * @param strategieString nom de la stratégie choisi par le joueur
     */
    public void setStrategie(String strategieString) {
        switch (strategieString) {
            case "naif":
				this.strategie = new Naif();
				break;
            case "rapide":
				this.strategie = new Rapide();
				break;
            case "expert":
                this.strategie = new Expert();
				break;
            case "tricheur":
                this.strategie = new Tricheur();
				break;
            case "humain":
                this.strategie = new Humain();
                break;
            default:
                throw new ConfigurationException("Stratégie Introuvable");
		}
    }

    /** Permet d'obtenir l'objet stratégie du joueur.
     * @return Objet Stratégie
     */
    public Strategie getStrategie() {
        return this.strategie;
    }
}
