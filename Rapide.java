package allumettes;

public class Rapide implements Strategie {
    /** Permet de choisir le nombre maximal d'allumettes.
     * @param jeu jeu sur lequelle s'applique la prise
     * @return nombre d'allumettes
     */
    @Override
    public int getPrise(Jeu jeu) {
        if (jeu.getNombreAllumettes() >= Jeu.PRISE_MAX) {
            return Jeu.PRISE_MAX;
        } else {
            return jeu.getNombreAllumettes();
        }
    }
}
