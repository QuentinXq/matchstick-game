package allumettes;

public class Expert implements Strategie {
    /** Permet de choisir le bon nombre d'allumettes pour gagner
     * la partie.
     * @param jeu jeu sur lequelle s'applique la prise
     * @return nombre d'allumettes
     */
    public int getPrise(Jeu jeu) {
        final int modulo = 4;
        if (((jeu.getNombreAllumettes() - 1) % modulo) != 0) {
            if (jeu.getNombreAllumettes() <= modulo) {
                return ((jeu.getNombreAllumettes() - 1) % modulo);
            } else {
                return (jeu.getNombreAllumettes() % modulo) - 1;
            }
        } else {
            return 1;
        }
    }
}
