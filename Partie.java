package allumettes;

/** Lance une partie des 13 allumettes en fonction des arguments fournis
 * sur la ligne de commande.
 * @author	Xavier Crégut
 * @version	$Revision: 1.5 $
 */
public class Partie {

	/** Lancer une partie. En argument sont donnés les deux joueurs sous
	 * la forme nom@stratégie.
	 * @param args Arguments fourni en paramètre
	 */
	public static void main(String[] args) {
		final int nombreAllumettes = 13;
		try {
			verifierNombreArguments(args);
			Arbitre arbitrePartie;
			Joueur joueur1;
			Joueur joueur2;
			Boolean confiant = false;
			if (args[0].equals("-confiant")) {
				joueur1 = Partie.ajoutJoueur(args[1]);
				joueur2 = Partie.ajoutJoueur(args[2]);
				confiant = true;
			} else {
				joueur1 = Partie.ajoutJoueur(args[0]);
				joueur2 = Partie.ajoutJoueur(args[1]);
			}
			arbitrePartie = new Arbitre(joueur1, joueur2);
			arbitrePartie.setConfiant(confiant);
			arbitrePartie.arbitrer(new JeuClassique(nombreAllumettes));
		} catch (ConfigurationException e) {
			System.out.println();
			System.out.println("Erreur : " + e.getMessage());
			afficherUsage();
			System.exit(1);
		}
	}

	/** Permet de vérifier le bon nombre d'arguments.
	 * @param args Arguments fourni en paramètre
	 */
	private static void verifierNombreArguments(String[] args) {
		final int nbJoueurs = 2;
		if (args.length < nbJoueurs) {
			throw new ConfigurationException("Trop peu d'arguments : "
					+ args.length);
		}
		if (args.length > nbJoueurs + 1) {
			throw new ConfigurationException("Trop d'arguments : "
					+ args.length);
		}
	}

	/** Afficher des indications sur la manière d'exécuter cette classe. */
	public static void afficherUsage() {
		System.out.println("\n" + "Usage :"
				+ "\n\t" + "java allumettes.Partie joueur1 joueur2"
				+ "\n\t\t" + "joueur est de la forme nom@stratégie"
				+ "\n\t\t"
				+ "strategie = naif | rapide | expert | humain | tricheur"
				+ "\n"
				+ "\n\t" + "Exemple :"
				+ "\n\t" + "	java allumettes.Partie Xavier@humain "
					   + "Ordinateur@naif"
				+ "\n"
				);
	}

	/** Permet de créer un joueur en fonction de 'nom@strategie'.
	 * @param nomStrategie
	 * @return Objet Joueur
	 */
	public static Joueur ajoutJoueur(String nomStrategie) {
		String[] nomStrategieArray = nomStrategie.split("@");
		String nom = nomStrategieArray[0];
		String stringStrategie = nomStrategieArray[1];
		Strategie strategie = null;
		switch (stringStrategie) {
            case "naif":
				strategie = new Naif();
				break;
            case "rapide":
				strategie = new Rapide();
				break;
            case "expert":
				strategie = new Expert();
				break;
            case "tricheur":
				strategie = new Tricheur();
				break;
            case "humain":
				strategie = new Humain();
				break;
			default:
				throw new
					ConfigurationException("Stratégie introuvable !");
		}
        return new Joueur(nom, strategie);
	}
}
