package allumettes;

import org.junit.*;
import static org.junit.Assert.*;

public class RapideTest {
    public static void main(String[] args) {
		org.junit.runner.JUnitCore.main("RapideTest");
    }

    int nombreAllumettes;
    Strategie strategieRapide = new Rapide();

    @Test
    public void tester3PlusAllumettes() {
        nombreAllumettes = 5;
        Jeu jeu = new JeuClassique(nombreAllumettes);
        assertEquals("tester3PlusAllumettes : Nombre d'allumettes prises différentes de 3", strategieRapide.getPrise(jeu), 3);
    }

    @Test
    public void tester3Allumettes() {
        nombreAllumettes = 3;
        Jeu jeu = new JeuClassique(nombreAllumettes);
        assertEquals("tester3Allumettes : Nombre d'allumettes prises différentes de 3", strategieRapide.getPrise(jeu), 3);
    }

    @Test
    public void tester2Allumettes() {
        nombreAllumettes = 2;
        Jeu jeu = new JeuClassique(nombreAllumettes);
        assertEquals("tester2Allumettes : Nombre d'allumettes prises différentes de 2", strategieRapide.getPrise(jeu), 2);
    }

    @Test
    public void tester1Allumette() {
        nombreAllumettes = 1;
        Jeu jeu = new JeuClassique(nombreAllumettes);
        assertEquals("tester1Allumette : Nombre d'allumettes prises différentes de 1", strategieRapide.getPrise(jeu), 1);
    }
}