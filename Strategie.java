package allumettes;

public interface Strategie {
    /** Permet de demander au joueur le nombre d'allumettes en fonction
     * de sa stratégie.
     * @param jeu jeu sur lequelle s'applique la prise
     * @return nombre d'allumettes
     */
    int getPrise(Jeu jeu);
}
