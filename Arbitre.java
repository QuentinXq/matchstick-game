package allumettes;

/** Modélise un joueur en fonction de son nom.
 * @author Quentin Rostalski
 * @version 1.0
 */
public class Arbitre {
    private Joueur joueur1;
    private Joueur joueur2;
    private Boolean confiant;

    /** Construit l'objet arbitre en fonction de deux joueurs.
     * @param j1 Joueur numéro 1
     * @param j2 Joueur numéro 2
     */
    public Arbitre(Joueur j1, Joueur j2) {
        this.joueur1 = j1;
        this.joueur2 = j2;
        this.confiant = false;
    }

    /** Permet d'arbitrer un jeu et de décider qui joue quand.
     * Permet également de donner le bon jeu aux joueurs.
     * @param jeu jeu sur lequel l'arbitre va arbitrer
     */
    public void arbitrer(Jeu jeu) {
        Joueur joueurQuiJoue = this.joueur1;
        int nombreAllumettes = jeu.getNombreAllumettes();
        boolean tourFini = false;
        Jeu leJeu = jeu;
        int priseJoueur;
        String gagnant;
        String perdant;
        do {
            try {
                System.out.println("Nombre d'allumettes restantes : "
                    + leJeu.getNombreAllumettes());
                System.out.println("Au tour de " + joueurQuiJoue.getNom() + ".");
                if (!this.confiant) {
                    Jeu jeuTriche = new JeuSansTriche(leJeu);
                    priseJoueur = joueurQuiJoue.getPrise(jeuTriche);
                } else {
                    priseJoueur = joueurQuiJoue.getPrise(leJeu);
                }
                if (priseJoueur == 1) {
                    System.out.println(joueurQuiJoue.getNom()
                        + " prend " + priseJoueur + " allumette.");
                } else {
                    System.out.println(joueurQuiJoue.getNom()
                        + " prend " + priseJoueur + " allumettes.");
                }
                leJeu.retirer(priseJoueur);
                if (leJeu.getNombreAllumettes() == 0) {
                    tourFini = true;
                    if (joueurQuiJoue == this.joueur1) {
                        gagnant = this.joueur2.getNom();
                    } else {
                        gagnant = this.joueur1.getNom();
                    }
                    System.out.println(joueurQuiJoue.getNom() + " a perdu !");
                    System.out.println(gagnant + " a gagné !");
                }
                System.out.println("");
                if (joueurQuiJoue == this.joueur1) {
                    joueurQuiJoue = this.joueur2;
                } else {
                    joueurQuiJoue = this.joueur1;
                }
            } catch (CoupInvalideException e) {
                System.out.println("Erreur ! " + e.getMessage());
                System.out.println("Recommencez !");
                System.out.println("");
            } catch (OperationInterditeException e) {
                System.out.println("Partie abandonnée car "
                    + joueurQuiJoue.getNom() + " a triché !");
                tourFini = true;
            }
        } while (!tourFini);
    }

    /** Permet de modifier la confiance de l'arbitre.
     * @param confiant
     */
    public void setConfiant(Boolean confiant) {
        this.confiant = confiant;
    }

    /** Permet d'obtenir l'objet Joueur du premier joueur.
     * @return Objet Joueur
     */
    public Joueur getJoueur1() {
        return this.joueur1;
    }

    /** Permet d'obtenir l'objet Joueur du second joueur.
     * @return Objet Joueur
     */
    public Joueur getJoueur2() {
        return this.joueur2;
    }
}
