package allumettes;

public class JeuClassique implements Jeu {
    private int nombreAllumettes;

    /** Construit l'objet Jeu en fonction du nombre d'allumettes.
     * @param nombreAllumettes
     */
    public JeuClassique(int nombreAllumettes) {
        this.nombreAllumettes = nombreAllumettes;
    }

    /** Permet d'obtenir le nombre d'allumettes.
     * @return
     */
    public int getNombreAllumettes() {
        return this.nombreAllumettes;
    }

    /** Permet de retirer des allumettes dans le jeu.
     * @param nombreAllumettesPrises
     * @throws CoupInvalideException
     */
    public void retirer(int nombreAllumettesPrises) throws CoupInvalideException {
        if (nombreAllumettesPrises > this.nombreAllumettes) {
            throw new CoupInvalideException(nombreAllumettesPrises, "> "
                + this.nombreAllumettes);
        } else if (nombreAllumettesPrises > Jeu.PRISE_MAX) {
            throw new CoupInvalideException(nombreAllumettesPrises, "> " + Jeu.PRISE_MAX);
        } else if (nombreAllumettesPrises < 1) {
            throw new CoupInvalideException(nombreAllumettesPrises, "< 1");
        } else {
            this.nombreAllumettes -= nombreAllumettesPrises;
        }
    }
}
